@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<form class="form-inline" method="POST" action="{{url('logic/store')}}">
			{{csrf_field()}}
			  <div class="form-group mb-2">
			    <label for="staticEmail2" class="sr-only">Email</label>
			    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Masukan Angka">
			  </div>
			  <div class="form-group mx-sm-3 mb-2">
			    <label for="angka" class="sr-only">Nilai Angka</label>
			    <input type="text" class="form-control" id="angka" name="angka" placeholder="angka">
			  </div>
			  <button type="submit" class="btn btn-success mb-2">Submit</button>
		</form>

		</div>
		<div class="col-md-6">
				<table class="table table-bordered">
				
				<tbody>
					@foreach ($results as $row)

						<tr>
							<td>
								{{$row}}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	
		</div>
	</div>
@endsection



