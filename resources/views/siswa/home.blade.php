@extends('layouts.app')
@section('content')

	<div class="container">

		<div class="row">
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-success custom-mb-20" data-toggle="modal" data-target="#addSiswa">
			  Tambah Data
			</button>

			<button type="button" class="btn btn-dark custom-mb-20 custom-mlr-10" data-toggle="modal" data-target="#addSekolah">
			  Tambah Sekolah
			</button>

			<div class="form-group">
              	 <select class="custom-select custom-select-md" name="sekolah_id">
				  <option selected>- List Sekolah -</option>
				  @foreach($sekolah as $row)
				  <option>{{$row->nama}}</option>
				  @endforeach
				</select>
              
            </div>

			<table class="table table-bordered">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nama</th>
			      <th scope="col">Alamat</th>
			      <th scope="col">No Telp</th>
			      <th scope="col">Sekolah</th>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($siswa as $row)
			    	<tr>
				      <th>{{$loop->iteration}}</th>
				      <td>{{$row->nama}}</td>
				      <td>{{$row->alamat}}</td>
				      <td>{{$row->no_telp}}</td>
				      <td>{{$row->sekolah->nama}}</td>
			    	</tr>
			    @endforeach
			  </tbody>
			</table>
			{{$siswa->links()}}
		</div>
	</div>


<!-- Modal -->
<div class="modal fade" id="addSiswa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formSave" method="POST" action="{{url('database/store')}}">
                        {{csrf_field()}}
                           <div class="form-group">
	                            <label for="nama">Nama</label>
	                            <input type="text" name="nama" class="form-control" id="nama" required="">     
                          </div>

                           <div class="form-group">
	                            <label for="alamat">Alamat</label>
	                            <input type="text" name="alamat" class="form-control" id="alamat" required="">     
                          </div>

                           <div class="form-group">
	                            <label for="no_telp">No Telp</label>
	                            <input type="text" name="no_telp" class="form-control" id="no_telp" required="">     
                          </div>

                          <div class="form-group">
	                            <label for="sekolah">Sekolah</label>
	                         
	                          	 <select class="custom-select custom-select-md" name="sekolah_id">
								  <option selected>-Pilih Sekolah-</option>
								  @foreach($sekolah as $row)
								  <option value="{{$row->id}}">{{$row->nama}}</option>
								  @endforeach
								</select>
	                          
                          </div>



                          <div class="modal-footer">
                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                          </div>
                    </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal add Sekolah -->

<!-- Modal -->
<div class="modal fade" id="addSekolah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Sekolah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{url('sekolah/store')}}">
        	 {{csrf_field()}}
           <div class="form-group">
                <label for="alamat">Nama Sekolah</label>
                <input type="text" name="nama" class="form-control" id="nama" required="">     
          </div>
           <div class="modal-footer">
            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
		</form>
      </div>
    </div>
  </div>
</div>

@endsection