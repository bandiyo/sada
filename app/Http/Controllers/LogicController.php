<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LogicController extends Controller
{
    public function home(){
        $user_id = Auth::id();
        // $user_name = Auth::user();
        // dd($user_name);
        // dd($user_id);
        if ($user_id==null) {
            return redirect('login');
        }else{
            return view('logic.home');
        }
        
    }
    public function store(Request $request){
        $angka =  $request->angka;

        $replace_titik = str_replace('.', '', $angka);
        $array = str_split($replace_titik);

        //cek length
        $length = strlen($replace_titik);
        $zero_length = strlen($replace_titik);

        $results = [];
        for ($i=0; $i <$length ; $i++) { 
            $zero_length--;
            $zero = null;
            for ($x=$zero_length; $x >0 ; $x--) { 
                $zero = $zero."0";
            }
            // echo $array[$i].$zero."<br>";
            array_push($results, $array[$i].$zero);
        }
        // var_dump($results);
        // dd($results);
        return view('logic.results',compact('results'));


    }
}
