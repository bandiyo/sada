<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sekolah;
use App\Siswa;
use Auth;
class SiswaController extends Controller
{
    public function home(){
    	$user_id = Auth::id();

    	// dd($user_id);
    	if ($user_id==null) {
    		return redirect('login');
    	}else{
    		$sekolah  = Sekolah::orderBy('id','desc')->get();
	    	$siswa = Siswa::orderBy('id','desc')->paginate(5);
	    	// dd($sekolah);
	    	return view('siswa.home',compact('siswa','sekolah'));	
    	}
    	
    }

    public function store(Request $request){
    	// dd($request);
    	$siswa = new Siswa;

    	$siswa->nama = $request->nama;
    	$siswa->alamat = $request->alamat;
    	$siswa->no_telp = $request->no_telp;
    	$siswa->sekolah_id = $request->sekolah_id;

    	$siswa->save();
    	return redirect('database');


    }
}
