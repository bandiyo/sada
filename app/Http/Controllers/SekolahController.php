<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sekolah;

class SekolahController extends Controller
{
     public function store(Request $request){
    	// dd($request);
    	$sekolah = new Sekolah;
    	$sekolah->nama = $request->nama;

    	$sekolah->save();

    	return redirect('database');

    }
}
