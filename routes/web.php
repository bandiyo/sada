<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('logic')->group(function () {
	Route::get('/','LogicController@home');
	Route::post('/store','LogicController@store');
});

Route::prefix('database')->group(function () {
	Route::get('/','SiswaController@home');
	Route::post('/store','SiswaController@store');
});

Route::prefix('sekolah')->group(function () {
	Route::get('/','SekolahController@home');
	Route::post('/store','SekolahController@store');
});